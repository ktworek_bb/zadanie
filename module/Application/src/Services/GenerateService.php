<?php

namespace Application\Services;

class GenerateService
{
    public function generateId($birthDate, $gender)
    {
        try {
            $pesel = '';
            $pesel .= $this->getDate($birthDate);
            $pesel .= $this->getSerialNumber();
            $pesel .= $this->getGender($gender);
            $pesel .= $this->getChecksum($pesel);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'msg' => $e->getMessage()
            ];
        }

        return [
            'success' => true,
            'id' => $pesel
        ];
    }

    private function getChecksum($pesel)
    {
        $dig = str_split($pesel);
        if (is_array($dig) && count($dig) === 10) {
            $code  = 9 * $dig[0];
            $code += 7 * $dig[1];
            $code += 3 * $dig[2];
            $code += 1 * $dig[3];
            $code += 9 * $dig[4];
            $code += 7 * $dig[5];
            $code += 3 * $dig[6];
            $code += 1 * $dig[7];
            $code += 9 * $dig[8];
            $code += 7 * $dig[9];

            return $code % 10;
        } else {
            throw new \Exception('Bład podczas liczenia sumy kontrolej!');
        }
    }

    private function getSerialNumber()
    {
        return sprintf("%03d", rand(0, 999));
    }

    private function getGender($gender)
    {
        $code = rand(0, 4);
        if ($gender === 'female') {
            $code *= 2;
        } else if ($gender === 'male') {
            $code = $code * 2 + 1;
        } else {
            throw new \Exception('Nieprawidłowe oznaczenie płci!');
        }

        return $code;
    }

    private function getDate($birthDate)
    {
        try {
            $date = new \DateTime($birthDate);

            $year  = $date->format('y');
            $year .= $this->applyMonthCorrection($date);
            $year .= $date->format('d');

            return $year;
        } catch (\Exception $e) {
            throw new \Exception('Nieprawidłowa data!');
        }
    }

    private function applyMonthCorrection($date)
    {
        $year = $date->format('Y');
        $month = $date->format('m');

        $yearPrefix = (int)(intval($year) / 100);

        $corrections = [
            '18' => 80,
            '19' => 0,
            '20' => 20,
            '21' => 40,
            '22' => 60,
        ];

        if (array_key_exists($yearPrefix, $corrections)) {
            $correctedMonth = intval($month) + $corrections[$yearPrefix];
            return sprintf("%02d", $correctedMonth);
        } else {
            throw new \Exception('Data urodzenia poza dopuszczalnym zakresem!');
        }
    }

}