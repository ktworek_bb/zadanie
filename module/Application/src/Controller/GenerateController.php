<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Application\Services\GenerateService;

class GenerateController extends AbstractActionController
{
    public function generateAction()
    {
        if ($this->getRequest()->isPost()) {
            $birthDate = $this->params()->fromPost('birth_date');
            $gender = $this->params()->fromPost('gender');

            $generator = new GenerateService();
            $result = $generator->generateId($birthDate, $gender);
        }

        return new JsonModel($result);
    }
}